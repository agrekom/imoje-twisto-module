<?php

namespace Imoje\Twisto\Controller\Payment;

include_once __DIR__ . "/../../../imoje-libs-module/PaymentCore/autoload.php";

use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Imoje\Payment\Configuration;
use Imoje\Payment\Util;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;

/**
 * Class Checkmethods
 *
 * @package Imoje\Twisto\Controller\Payment
 */
class Checkmethods extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface
{

	/**
	 * @inheritDoc
	 */
	public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
	{
		return null;
	}

	/**
	 * @inheritDoc
	 */
	public function validateForCsrf(RequestInterface $request): ?bool
	{
		return true;
	}

	/**
	 * @var Order
	 */
	protected $order;

	/**
	 * @var ScopeConfigInterface
	 */
	protected $scopeConfig;

	/**
	 * @var BuilderInterface
	 */
	protected $builderInterface;

	/**
	 * @var WriterInterface
	 */
	protected $configWriter;

	/**
	 * @var TypeListInterface
	 */
	protected $cacheTypeList;

	/**
	 * ReturnUrl constructor.
	 *
	 * @param Context $context
	 */
	public function __construct(
		Context $context,
		Order $order,
		ScopeConfigInterface $scopeConfig,
		BuilderInterface $builderInterface,
		WriterInterface $configWriter,
		TypeListInterface $cacheTypeList
	) {
		parent::__construct($context);
		$this->order = $order;
		$this->scopeConfig = $scopeConfig;
		$this->builderInterface = $builderInterface;
		$this->configWriter = $configWriter;
		$this->cacheTypeList = $cacheTypeList;
	}

	/**
	 * @param string $value
	 *
	 * @return string
	 */
	private function getConfigValue($value)
	{
		return $this->scopeConfig->getValue($value, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

	/**
	 * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
	 * @throws \Exception
	 */
	public function execute()
	{

		$algoHash = 'sha256';
		$postData = json_encode([]);

		$signature =
			'merchantid=' . $this->getConfigValue('payment/imoje_twisto/merchant_id')
			. ';serviceid=' . $this->getConfigValue('payment/imoje_twisto/service_id')
			. ';signature=' . Util::hashSignature($algoHash, $postData, $this->getConfigValue('payment/imoje_twisto/service_key'))
			. ';alg=' . $algoHash;

		$curlInit = curl_init(Configuration::getServiceUrl($this->getConfigValue('payment/imoje_twisto/sandbox')
				? Util::ENVIRONMENT_SANDBOX
				: Util::ENVIRONMENT_PRODUCTION) . Configuration::ROUTE_CHECK_PAYMENT_METHODS);
		curl_setopt($curlInit, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($curlInit, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($curlInit, CURLOPT_TIMEOUT, 10);
		curl_setopt($curlInit, CURLOPT_HTTPHEADER, [
			"Content-Type: application/json",
			"X-Imoje-Signature: " . $signature,
		]);

		$resultCurl = curl_exec($curlInit);
		$httpStatus = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);

		if(!$resultCurl
			|| $httpStatus != 200
			|| !Util::isJson($resultCurl)) {

			$this->configWriter->save('payment/imoje_twisto/error_tmp', 1, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);

			$this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
			$this->cacheTypeList->cleanType(\Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER);

			$this->_redirect($this->_redirect->getRefererUrl());

			return;
		}

		$curlResponse = json_decode($resultCurl, true);

		if(isset($curlResponse['status'])
			&& $curlResponse['status'] !== 'ok') {

			$this->configWriter->save('payment/imoje_twisto/error_tmp', 1, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);

			$this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
			$this->cacheTypeList->cleanType(\Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER);

			$this->_redirect($this->_redirect->getRefererUrl());

			return;
		}

		$this->configWriter->save('payment/imoje_twisto/system_config_twisto', 0, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);

		$paymentMethods = [];

		if(isset($curlResponse['data']['twisto'])
			&& $curlResponse['data']['twisto']['enable']) {

			$paymentMethods['twisto'] = [
				'sk'      => $curlResponse['data']['twisto']['sk'],
				'pk'      => $curlResponse['data']['twisto']['pk'],
				'timeout' => $curlResponse['data']['twisto']['timeout'],
			];
			$this->configWriter->save('payment/imoje_twisto/system_config_twisto', 1, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0); // must be, because Magento can show (via js) option (depends) without saving configuration
		} else {
			$this->configWriter->save('payment/imoje_twisto/no_twisto', 1, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);

		}
		$this->configWriter->save('payment/imoje_twisto/payment_methods', json_encode($paymentMethods), ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
		$this->configWriter->save('payment/imoje_twisto/success_tmp', 1, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);

		$this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
		$this->cacheTypeList->cleanType(\Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER);

		$this->_redirect($this->_redirect->getRefererUrl());

		return;
	}
}


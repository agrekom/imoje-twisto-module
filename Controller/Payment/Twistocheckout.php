<?php

namespace Imoje\Twisto\Controller\Payment;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Twistocheckout
 *
 * @package Imoje\Twisto\Controller\Payment
 */
class Twistocheckout extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface
{

	/**
	 * @inheritDoc
	 */
	public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
	{
		return null;
	}

	/**
	 * @inheritDoc
	 */
	public function validateForCsrf(RequestInterface $request): ?bool
	{
		return true;
	}

	/**
	 *
	 */
	public function execute()
	{

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		if(!isset($_POST['orderId'])) {

			return $this->resultRedirectFactory->create()->setUrl($objectManager->get('\Magento\Store\Model\StoreManagerInterface')->getStore()->getBaseUrl());;
		}

		/** @var \Magento\Framework\View\Layout $layout */
		$layout = $this->_view->getLayout();

		/** @var Imoje\Twisto\Block\Checkout $block */
		$block = $layout->createBlock(\Imoje\Twisto\Block\Checkout::class);
		$block->setTemplate('Imoje_Twisto::redirect-with-twisto.phtml');

		$this->getResponse()->setBody($block->toHtml());
	}
}


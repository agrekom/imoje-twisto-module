<?php

namespace Imoje\Twisto\Model\Payment;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractExtensibleModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Payment\Model\MethodInterface;

/**
 * Class AbstractPaymentGateway
 *
 * @package Imoje\Twisto\Model\Payment
 */
abstract class AbstractPaymentGateway extends AbstractExtensibleModel implements MethodInterface
{

	const STATUS_UNKNOWN = 'UNKNOWN';

	const STATUS_APPROVED = 'APPROVED';

	const STATUS_ERROR = 'ERROR';

	const STATUS_DECLINED = 'DECLINED';

	const STATUS_VOID = 'VOID';

	const STATUS_SUCCESS = 'SUCCESS';

	/**
	 * @var string
	 */
	protected $code;

	/**
	 * @var string
	 */
	protected $infoBlockType = \Magento\Payment\Block\Info::class;

	/**
	 * @var string
	 */
	protected $formBlockType = \Magento\Payment\Block\Form::class;

	/**
	 * @var int
	 */
	protected $storeId;

	/**
	 * @var Data
	 */
	protected $paymentData;

	/**
	 * @var ScopeConfigInterface
	 */
	protected $scopeConfig;

	/**
	 * @var Logger
	 */
	protected $logger;

	/**
	 * @var String
	 */
	protected $title;

	/**
	 * @param Context                    $context
	 * @param Registry                   $registry
	 * @param ExtensionAttributesFactory $extensionFactory
	 * @param AttributeValueFactory      $customAttributeFactory
	 * @param Data                       $paymentData
	 * @param ScopeConfigInterface       $scopeConfig
	 * @param Logger                     $logger
	 *
	 * @param AbstractResource|null      $resource
	 * @param AbstractDb|null            $resourceCollection
	 * @param array                      $data
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
	 */
	public function __construct(
		Context $context,
		Registry $registry,
		ExtensionAttributesFactory $extensionFactory,
		AttributeValueFactory $customAttributeFactory,
		Data $paymentData,
		ScopeConfigInterface $scopeConfig,
		Logger $logger,
		AbstractResource $resource = null,
		AbstractDb $resourceCollection = null,
		array $data = []
	) {
		parent::__construct(
			$context,
			$registry,
			$extensionFactory,
			$customAttributeFactory,
			$resource,
			$resourceCollection,
			$data
		);

		$this->paymentData = $paymentData;
		$this->scopeConfig = $scopeConfig;
		$this->logger = $logger;

		$this->initializeData($data);
	}

	/**
	 * Initializes injected data
	 *
	 * @param array $data
	 *
	 * @return void
	 */
	protected function initializeData($data = [])
	{
		if(!empty($data['formBlockType'])) {
			$this->formBlockType = $data['formBlockType'];
		}
	}

	/**
	 * Retrieve payment method code
	 *
	 * @return string
	 *
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * Retrieve block type for method form generation
	 *
	 * @return string
	 *
	 * @deprecated
	 */
	public function getFormBlockType()
	{
		return $this->formBlockType;
	}

	/**
	 * Retrieve payment method title
	 *
	 * @return string
	 *
	 */
	public function getTitle()
	{
		return $this->getConfigData('title');
	}

	/**
	 * Store id setter
	 *
	 * @param int $storeId
	 *
	 * @return void
	 */
	public function setStore($storeId)
	{
		$this->storeId = (int) $storeId;
	}

	/**
	 * Store id getter
	 *
	 * @return int
	 */
	public function getStore()
	{
		return $this->storeId;
	}

	/**
	 * Check order availability
	 *
	 * @return bool
	 *
	 */
	public function canOrder()
	{
		return true;
	}

	/**
	 * Check authorize availability
	 *
	 * @return bool
	 *
	 */
	public function canAuthorize()
	{
		return false;
	}

	/**
	 * Check capture availability
	 *
	 * @return bool
	 *
	 */
	public function canCapture()
	{
		return true;
	}

	/**
	 * Check partial capture availability
	 *
	 * @return bool
	 *
	 */
	public function canCapturePartial()
	{
		return false;
	}

	/**
	 * Check whether capture can be performed once and no further capture possible
	 *
	 * @return bool
	 *
	 */
	public function canCaptureOnce()
	{
		return true;
	}

	/**
	 * Check refund availability
	 *
	 * @return bool
	 *
	 */
	public function canRefund()
	{
		return false;
	}

	/**
	 * Check partial refund availability for invoice
	 *
	 * @return bool
	 *
	 */
	public function canRefundPartialPerInvoice()
	{
		return false;
	}

	/**
	 * Check void availability
	 *
	 * @return bool
	 *
	 */
	public function canVoid()
	{
		return true;
	}

	/**
	 * Using internal pages for input payment data
	 * Can be used in admin
	 *
	 * @return bool
	 */
	public function canUseInternal()
	{
		return false;
	}

	/**
	 * Can be used in regular checkout
	 *
	 * @return bool
	 */
	public function canUseCheckout()
	{
		return true;
	}

	/**
	 * Can be edit order (renew order)
	 *
	 * @return bool
	 *
	 */
	public function canEdit()
	{
		return false;
	}

	/**
	 * Check fetch transaction info availability
	 *
	 * @return bool
	 *
	 */
	public function canFetchTransactionInfo()
	{
		return false;
	}

	/**
	 * Fetch transaction info
	 *
	 * @param InfoInterface $payment
	 * @param string        $transactionId
	 *
	 * @return array
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 *
	 */
	public function fetchTransactionInfo(InfoInterface $payment, $transactionId)
	{
		return [];
	}

	/**
	 * Retrieve payment system relation flag
	 *
	 * @return bool
	 *
	 */
	public function isGateway()
	{
		return true;
	}

	/**
	 * Retrieve payment method online/offline flag
	 *
	 * @return bool
	 *
	 */
	public function isOffline()
	{
		return false;
	}

	/**
	 * Flag if we need to run payment initialize while order place
	 *
	 * @return bool
	 *
	 */
	public function isInitializeNeeded()
	{
		return true;
	}

	/**
	 * To check billing country is allowed for the payment method
	 *
	 * @param string $country
	 *
	 * @return bool
	 */
	public function canUseForCountry($country)
	{
		return true;
	}

	/**
	 * Check method for processing with base currency
	 *
	 * @param string $currencyCode
	 *
	 * @return bool
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function canUseForCurrency($currencyCode)
	{
		return true;
	}

	/**
	 * Retrieve block type for display method information
	 *
	 * @return string
	 *
	 * @deprecated
	 */
	public function getInfoBlockType()
	{
		return $this->infoBlockType;
	}

	/**
	 * Retrieve payment information model object
	 *
	 * @return InfoInterface
	 * @throws LocalizedException
	 *
	 */
	public function getInfoInstance()
	{
		return $this->getData('info_instance');
	}

	/**
	 * Retrieve payment information model object
	 *
	 * @param InfoInterface $info
	 *
	 * @return void
	 *
	 * @deprecated
	 */
	public function setInfoInstance(InfoInterface $info)
	{
		$this->setData('info_instance', $info);
	}

	/**
	 * Validate payment method information object
	 *
	 * @return $this
	 * @throws LocalizedException
	 *
	 */
	public function validate()
	{
		return $this;
	}

	/**
	 * Order payment method
	 *
	 * @param InfoInterface $payment
	 * @param float         $amount
	 *
	 * @return $this
	 * @throws LocalizedException
	 */
	public function order(InfoInterface $payment, $amount)
	{
		if(!$this->canOrder()) {
			throw new LocalizedException(__('The order action is not available.'));
		}

		return $this;
	}

	/**
	 * Authorize payment method
	 *
	 * @param InfoInterface $payment
	 * @param float         $amount
	 *
	 * @return $this
	 * @throws LocalizedException
	 */
	public function authorize(InfoInterface $payment, $amount)
	{
		if(!$this->canAuthorize()) {
			throw new LocalizedException(__('The authorize action is not available.'));
		}

		return $this;
	}

	/**
	 * Capture payment method
	 *
	 * @param InfoInterface $payment
	 * @param float         $amount
	 *
	 * @return $this
	 * @throws LocalizedException
	 */
	public function capture(InfoInterface $payment, $amount)
	{
		if(!$this->canCapture()) {
			throw new LocalizedException(__('The capture action is not available.'));
		}

		return $this;
	}

	/**
	 * Refund specified amount for payment
	 *
	 * @param InfoInterface $payment
	 * @param float         $amount
	 *
	 * @return $this
	 * @throws LocalizedException
	 */
	public function refund(InfoInterface $payment, $amount)
	{
		if(!$this->canRefund()) {
			throw new LocalizedException(__('The refund action is not available.'));
		}

		return $this;
	}

	/**
	 * Cancel payment method
	 *
	 * @param InfoInterface $payment
	 *
	 * @return $this
	 *
	 */
	public function cancel(InfoInterface $payment)
	{
		return $this;
	}

	/**
	 * Void payment method
	 *
	 * @param InfoInterface $payment
	 *
	 * @return $this
	 * @throws LocalizedException
	 */
	public function void(InfoInterface $payment)
	{
		if(!$this->canVoid()) {
			throw new LocalizedException(__('The void action is not available.'));
		}

		return $this;
	}

	/**
	 * Whether this method can accept or deny payment
	 *
	 * @return bool
	 *
	 */
	public function canReviewPayment()
	{
		return true;
	}

	/**
	 * Attempt to accept a payment that us under review
	 *
	 * @param InfoInterface $payment
	 *
	 * @return false
	 * @throws LocalizedException
	 *
	 */
	public function acceptPayment(InfoInterface $payment)
	{
		if(!$this->canReviewPayment()) {
			throw new LocalizedException(__('The payment review action is unavailable.'));
		}

		return false;
	}

	/**
	 * Attempt to deny a payment that us under review
	 *
	 * @param InfoInterface $payment
	 *
	 * @return false
	 * @throws LocalizedException
	 *
	 */
	public function denyPayment(InfoInterface $payment)
	{
		if(!$this->canReviewPayment()) {
			throw new LocalizedException(__('The payment review action is unavailable.'));
		}

		return false;
	}

	/**
	 * Retrieve information from payment configuration
	 *
	 * @param string                                     $field
	 * @param int|string|null|\Magento\Store\Model\Store $storeId
	 *
	 * @return mixed
	 */
	public function getConfigData($field, $storeId = null)
	{
		if(null === $storeId) {
			$storeId = $this->getStore();
		}
		$path = 'payment/' . $this->getCode() . '/' . $field;

		return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
	}

	/**
	 * Assign data to info model instance
	 *
	 * @param DataObject $data
	 *
	 * @return $this
	 *
	 */
	public function assignData(DataObject $data)
	{
		return $this;
	}

	/**
	 * Check whether payment method can be used
	 *
	 * @param CartInterface|null $quote
	 *
	 * @return bool
	 *
	 */
	public function isAvailable(CartInterface $quote = null)
	{
		if(!$this->isActive($quote
			? $quote->getStoreId()
			: null)
		) {
			return false;
		}

		$checkResult = new DataObject();
		$checkResult->setData('is_available', true);

		$this->_eventManager->dispatch(
			'payment_method_is_active',
			[
				'result' => $checkResult,
				'method_instance' => $this,
				'quote' => $quote
			]
		);

		return $checkResult->getData('is_available');
	}

	/**
	 * Is active
	 *
	 * @param int|null $storeId
	 *
	 * @return bool
	 *
	 */
	public function isActive($storeId = null)
	{
		return (bool) (int) $this->getConfigData('active', $storeId);
	}

	/**
	 * Method that will be executed instead of authorize or capture
	 * if flag isInitializeNeeded set to true
	 *
	 * @param string $paymentAction
	 * @param object $stateObject
	 *
	 * @return $this
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 *
	 */
	public function initialize($paymentAction, $stateObject)
	{
		return $this;
	}

	/**
	 * Get config payment action url
	 * Used to universalize payment actions when processing payment place
	 *
	 * @return string
	 *
	 */
	public function getConfigPaymentAction()
	{
		return $this->getConfigData('payment_action');
	}
}

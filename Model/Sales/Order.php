<?php

namespace Imoje\Twisto\Model\Sales;

/**
 * Class Order
 *
 * @package Imoje\Twisto\Model\Sales
 */
class Order extends \Magento\Sales\Model\Order
{
	/**
	 * Order constructor.
	 *
	 * @param \Magento\Framework\Model\Context                                          $context
	 * @param \Magento\Framework\Registry                                               $registry
	 * @param \Magento\Framework\Api\ExtensionAttributesFactory                         $extensionFactory
	 * @param \Magento\Framework\Api\AttributeValueFactory                              $customAttributeFactory
	 * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface                      $timezone
	 * @param \Magento\Store\Model\StoreManagerInterface                                $storeManager
	 * @param \Magento\Sales\Model\Order\Config                                         $orderConfig
	 * @param \Magento\Catalog\Api\ProductRepositoryInterface                           $productRepository
	 * @param \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory           $orderItemCollectionFactory
	 * @param \Magento\Catalog\Model\Product\Visibility                                 $productVisibility
	 * @param \Magento\Sales\Api\InvoiceManagementInterface                             $invoiceManagement
	 * @param \Magento\Directory\Model\CurrencyFactory                                  $currencyFactory
	 * @param \Magento\Eav\Model\Config                                                 $eavConfig
	 * @param \Magento\Sales\Model\Order\Status\HistoryFactory                          $orderHistoryFactory
	 * @param \Magento\Sales\Model\ResourceModel\Order\Address\CollectionFactory        $addressCollectionFactory
	 * @param \Magento\Sales\Model\ResourceModel\Order\Payment\CollectionFactory        $paymentCollectionFactory
	 * @param \Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory $historyCollectionFactory
	 * @param \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory        $invoiceCollectionFactory
	 * @param \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory       $shipmentCollectionFactory
	 * @param \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory     $memoCollectionFactory
	 * @param \Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory $trackCollectionFactory
	 * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory                $salesOrderCollectionFactory
	 * @param \Magento\Framework\Pricing\PriceCurrencyInterface                         $priceCurrency
	 * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory            $productListFactory
	 * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null              $resource
	 * @param \Magento\Framework\Data\Collection\AbstractDb|null                        $resourceCollection
	 * @param array                                                                     $data
	 */
	public function __construct(
		\Magento\Framework\Model\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
		\Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
		\Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Sales\Model\Order\Config $orderConfig,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		\Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollectionFactory,
		\Magento\Catalog\Model\Product\Visibility $productVisibility,
		\Magento\Sales\Api\InvoiceManagementInterface $invoiceManagement,
		\Magento\Directory\Model\CurrencyFactory $currencyFactory,
		\Magento\Eav\Model\Config $eavConfig,
		\Magento\Sales\Model\Order\Status\HistoryFactory $orderHistoryFactory,
		\Magento\Sales\Model\ResourceModel\Order\Address\CollectionFactory $addressCollectionFactory,
		\Magento\Sales\Model\ResourceModel\Order\Payment\CollectionFactory $paymentCollectionFactory,
		\Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory $historyCollectionFactory,
		\Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory,
		\Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $shipmentCollectionFactory,
		\Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory $memoCollectionFactory,
		\Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory $trackCollectionFactory,
		\Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesOrderCollectionFactory,
		\Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productListFactory,
		\Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
		\Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
		array $data = []
	) {
		parent::__construct(
			$context,
			$registry,
			$extensionFactory,
			$customAttributeFactory,
			$timezone,
			$storeManager,
			$orderConfig,
			$productRepository,
			$orderItemCollectionFactory,
			$productVisibility,
			$invoiceManagement,
			$currencyFactory,
			$eavConfig,
			$orderHistoryFactory,
			$addressCollectionFactory,
			$paymentCollectionFactory,
			$historyCollectionFactory,
			$invoiceCollectionFactory,
			$shipmentCollectionFactory,
			$memoCollectionFactory,
			$trackCollectionFactory,
			$salesOrderCollectionFactory,
			$priceCurrency,
			$productListFactory,
			$resource,
			$resourceCollection,
			$data
		);
	}
}

<?php

namespace Imoje\Twisto\Model;

include_once __DIR__ . "/../../imoje-libs-module/PaymentCore/autoload.php";

use Imoje\Payment\Api;
use Imoje\Payment\Payment;
use Imoje\Payment\Util;
use Imoje\Twisto\Model\Payment\AbstractPaymentGateway;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository as AssetRepository;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\Method\Logger;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Twisto
 *
 * @package Imoje\Twisto\Model
 */
class Twisto extends AbstractPaymentGateway
{
	const CODE = 'imoje_twisto';

	/**
	 * @var UrlInterface
	 */
	protected $urlBuilder;

	/**
	 * @var string
	 */
	protected $code = self::CODE;

	/**
	 * @var Order
	 */
	private $order;

	/**
	 * @var StoreManagerInterface
	 */
	private $storeManager;

	/**
	 * @var Payment
	 */
	private $paymentGateway;

	/**
	 * @var AssetRepository
	 */
	private $assetRepository;

	/**
	 * Twisto constructor.
	 *
	 * @param Context                    $context
	 * @param Registry                   $registry
	 * @param ExtensionAttributesFactory $extensionFactory
	 * @param AttributeValueFactory      $customAttributeFactory
	 * @param Data                       $paymentData
	 * @param ScopeConfigInterface       $scopeConfig
	 * @param Logger                     $logger
	 * @param UrlInterface               $urlBuilder
	 * @param Order                      $order
	 * @param StoreManagerInterface      $storeManager
	 * @param AssetRepository            $assetRepository
	 * @param AbstractResource|null      $resource
	 * @param AbstractDb|null            $resourceCollection
	 * @param array                      $data
	 */
	public function __construct(
		Context $context,
		Registry $registry,
		ExtensionAttributesFactory $extensionFactory,
		AttributeValueFactory $customAttributeFactory,
		Data $paymentData,
		ScopeConfigInterface $scopeConfig,
		Logger $logger,
		UrlInterface $urlBuilder,
		Order $order,
		StoreManagerInterface $storeManager,
		AssetRepository $assetRepository,
		AbstractResource $resource = null,
		AbstractDb $resourceCollection = null,
		array $data = []
	) {
		parent::__construct(
			$context,
			$registry,
			$extensionFactory,
			$customAttributeFactory,
			$paymentData,
			$scopeConfig,
			$logger,
			$resource,
			$resourceCollection,
			$data
		);

		$this->assetRepository = $assetRepository;
		$this->urlBuilder = $urlBuilder;
		$this->order = $order;
		$this->storeManager = $storeManager;

		$this->setStore($this->storeManager->getStore()->getId());

		$this->paymentGateway = new Payment(
			$this->getConfigData('sandbox')
				? Util::ENVIRONMENT_SANDBOX
				: Util::ENVIRONMENT_PRODUCTION,
			$this->getConfigData('service_key'),
			$this->getConfigData('service_id'),
			$this->getConfigData('merchant_id')
		);
	}

	/**
	 * @return bool
	 */
	public function canRefund()
	{
		return true;
	}

	/**
	 * @param \Magento\Payment\Model\InfoInterface $payment
	 * @param float                                $amount
	 *
	 * @return Twisto
	 */
	public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
	{

		$api = new Api($this->getConfigData('authorization_token'), $this->getConfigData('merchant_id'), $this->getConfigData('service_id'));

		$refund = $api->createRefund(
			$api->prepareRefundData(
				Util::convertAmountToFractional($amount)
			),
			$payment->getParentTransactionId()
		);

		if($refund['success']) {

			return $this;
		}

		throw new \Magento\Framework\Exception\LocalizedException(__('The refund action is not available at this moment. Try again later.'));
	}

	/**
	 * @return string
	 */
	public function getCheckoutRedirectUrl()
	{

		return $this->urlBuilder->getUrl('imoje_twisto/payment/checkout');
	}

	/**
	 * @return string
	 */
	public function getPaymentLogoSrc()
	{

		return $this->getConfigData('hide_brand')
			? ''
			: $this->assetRepository->getUrl('Imoje_Twisto::images/logo.png');
	}

	/**
	 * @return Payment
	 */
	public function getPaymentGateway()
	{
		return $this->paymentGateway;
	}

	/**
	 * @param string $currencyCode
	 *
	 * @return bool
	 */
	public function canUseForCurrency($currencyCode)
	{

		$currencies = $this->getConfigData('currency');

		if(!is_string($currencies) || !$currencies) {
			return false;
		}

		return Util::canUseForCurrency(
				strtolower($currencyCode),
				explode(
					',',
					strtolower(
						str_replace(' ', '', $currencies)
					)
				)
			)
			&& $this->getConfigData('service_key')
			&& $this->getConfigData('service_id')
			&& $this->getConfigData('merchant_id');
	}

	/**
	 * @return mixed
	 */
	public function getLocale()
	{
		return $this->scopeConfig->getValue(
			'general/locale/code',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
			$this->getStore()
		);
	}
}

<?php

namespace Imoje\Twisto\Block;

include_once __DIR__ . "/../../imoje-libs-module/PaymentCore/autoload.php";
include_once __DIR__ . "/../../imoje-libs-module/PaymentCore/vendor/twistopayments/twisto.php/src/Twisto.php";

use Imoje\Payment\Payment;
use Imoje\Payment\Twisto;
use Imoje\Payment\Util;
use Imoje\Twisto\Model\Order;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\ResourceModel\Order\Tax\Item;
use Magento\Store\Api\Data\StoreInterface;

/**
 * Class Checkout
 *
 * @package Imoje\Twisto\Block
 */
class Checkout extends Template
{

	/**
	 * @var \Magento\Framework\UrlInterface
	 */
	public $urlBuilder;

	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $scopeConfig;

	/**
	 * @var OrderRepository
	 */
	protected $orderRepository;

	/**
	 * @var Registry
	 */
	private $registry;

	/**
	 * @var Order
	 */
	private $order;

	/**
	 * @var StoreInterface
	 */
	private $store;

	/**
	 * @var Cart
	 */
	private $cartHelper;

	/**
	 * @var ResponseFactory
	 */
	private $responseFactory;

	/**
	 * @var OrderFactory
	 */
	private $searchCriteriaBuilder;

	/**
	 * @var FilterBuilder
	 */
	private $filterBuilder;

	/**
	 * @var FilterGroupBuilder
	 */
	private $filterGroupBuilder;

	/**
	 * @var \Magento\Framework\App\ProductMetadataInterface $productMetadataInterface
	 */
	private $productMetadataInterface;

	/**
	 * @var Magento\Sales\Model\ResourceModel\Order\Tax\Item $taxItem
	 */
	private $taxItem;

	/**
	 * Checkout constructor.
	 *
	 * @param Context                                            $context
	 * @param Order                                              $order
	 * @param Registry                                           $registry
	 * @param StoreInterface                                     $store
	 * @param \Magento\Framework\UrlInterface                    $urlBuilder
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \Magento\Checkout\Helper\Cart                      $cartHelper
	 * @param \Magento\Framework\App\ResponseFactory             $responseFactory
	 * @param \Magento\Framework\Api\SearchCriteriaBuilder       $searchCriteriaBuilder
	 * @param \Magento\Sales\Model\OrderRepository               $orderRepository
	 * @param \Magento\Framework\Api\FilterBuilder               $filterBuilder
	 * @param \Magento\Framework\Api\Search\FilterGroupBuilder   $filterGroupBuilder
	 * @param \Magento\Framework\App\ProductMetadataInterface    $productMetadataInterface
	 * @param \Magento\Sales\Model\ResourceModel\Order\Tax\Item  $taxItem
	 *
	 * @internal param OrderFactory $orderFactory
	 */
	public function __construct(
		Context $context,
		Order $order,
		Registry $registry,
		StoreInterface $store,
		\Magento\Framework\UrlInterface $urlBuilder,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Checkout\Helper\Cart $cartHelper,
		\Magento\Framework\App\ResponseFactory $responseFactory,
		\Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
		\Magento\Sales\Model\OrderRepository $orderRepository,
		\Magento\Framework\Api\FilterBuilder $filterBuilder,
		\Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder,
		\Magento\Framework\App\ProductMetadataInterface $productMetadataInterface,
		\Magento\Sales\Model\ResourceModel\Order\Tax\Item $taxItem
	) {
		parent::__construct($context);
		$this->registry = $registry;
		$this->order = $order;
		$this->store = $store;
		$this->urlBuilder = $urlBuilder;
		$this->scopeConfig = $scopeConfig;
		$this->cartHelper = $cartHelper;
		$this->responseFactory = $responseFactory;
		$this->searchCriteriaBuilder = $searchCriteriaBuilder;
		$this->orderRepository = $orderRepository;
		$this->filterBuilder = $filterBuilder;
		$this->filterGroupBuilder = $filterGroupBuilder;
		$this->productMetadataInterface = $productMetadataInterface;
		$this->taxItem = $taxItem;

		if($this->getOrderState($this->getOrderIdFromPost()) === \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT) {

			$this->redirectToHomePage();
			die();
		}
	}

	/**
	 * @param array $customerEmail
	 *
	 * @return mixed
	 */
	public function getPreviousOrders($customerEmail)
	{

		$orders = $this->searchCriteriaBuilder
			->addFilter('customer_email', $customerEmail, 'eq')
			->addFilter('status', [
				'complete',
				'closed',
			], 'in')
			->create();

		$orders = $this->orderRepository->getList($orders);

		if(empty($orders)) {
			return [];
		}

		// define how many objects will be in array
		$maxElements = 1;
		$counter = 0;

		$twistoOrders = [];

		foreach($orders as $k => $order) {

			$counter += 1;

			$cart = $this->getCart($order);

			$addressBilling = $cart['addressBilling'];
			$addressDelivery = $cart['addressDelivery'];
			$items = $cart['items'];

			$twistoItems = [];
			foreach($items as $product) {

				if(isset($twistoItems[$product['productId']])) {

					$twistoItems[$product['productId']]->quantity += $product['quantity'];
					$twistoItems[$product['productId']]->price_vat += round($product['quantity'] * $product['amount'], 2);
				} else {
					$twistoItems[] = new \Twisto\Item(
						\Twisto\Item::TYPE_DEFAULT,
						Twisto::getItemName($product['name']),
						$product['productId'],
						$product['quantity'],
						Util::multiplyValues($product['amount'], $product['quantity'], 2),
						$product['vatRate']
					);
				}
			}

			$total = 0;
			foreach($twistoItems as $twistoItem) {
				$total += $twistoItem->price_vat;
			}

			$twistoOrders[] = new \Twisto\Order(
				\DateTime::createFromFormat('Y-m-d H:i:s', $order->getCreatedAt()),
				new \Twisto\Address($addressBilling['name'],
					$addressBilling['street'],
					$addressBilling['city'],
					str_replace('-', '', $addressBilling['postalCode']),
					$addressBilling['countryCode'],
					$addressBilling['phone']),
				new \Twisto\Address($addressDelivery['name'],
					$addressDelivery['street'],
					$addressDelivery['city'],
					str_replace('-', '', $addressDelivery['postalCode']),
					$addressDelivery['countryCode'],
					$addressDelivery['phone']),
				$total,
				$twistoItems

			);

			if($counter === $maxElements) {
				break;
			}
		}

		return $twistoOrders;
	}

	/**
	 * @return bool
	 */
	public function getTwistoStatus()
	{
		return Twisto::isActive(
			$this->getPaymentMethods(),
			$this->getConfigValue('payment/imoje_twisto/twisto')
		);
	}

	/**
	 * That function redirect to homepage
	 *
	 * @return mixed
	 */
	public function redirectToHomePage()
	{
		return $this->responseFactory->create()->setRedirect($this->urlBuilder->getUrl())->sendResponse(); //Redirect to cms page
	}

	/**
	 * @return string
	 */
	public function getEnvironment()
	{
		return $this->getConfigValue('payment/imoje_twisto/sandbox') == 1
			? Util::ENVIRONMENT_SANDBOX
			: Util::ENVIRONMENT_PRODUCTION;
	}

	/**
	 * @return array
	 */
	public function getPaymentMethods()
	{

		return json_decode($this->getConfigValue('payment/imoje_twisto/payment_methods'), true);
	}

	/**
	 * @param string $orderId
	 *
	 * @return mixed
	 */
	public function getOrderState($orderId = '')
	{
		return $this->getOrderObject($orderId)->getState();
	}

	/**
	 * @param string $orderId
	 *
	 * @return string
	 */
	public function renderForm($orderId = '')
	{

		if(empty($orderId)) {
			$orderId = $this->getOrderId();
		}

		$order = $this->getOrderObject($orderId);

		$order->setState(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
		$order->save();

		/**
		 * Get Magento version.
		 */
		$version = $this->productMetadataInterface->getVersion();

		$payment = new Payment(
			$this->getConfigValue('payment/imoje_twisto/sandbox') == 1
				? Util::ENVIRONMENT_SANDBOX
				: Util::ENVIRONMENT_PRODUCTION,
			$this->getConfigValue('payment/imoje_twisto/service_key'),
			$this->getConfigValue('payment/imoje_twisto/service_id'),
			$this->getConfigValue('payment/imoje_twisto/merchant_id')
		);

		$twistoData = '';

		if($this->getTwistoStatus()) {
			$twistoData = $this->getTwistoDataFromPost();

			if($this->getEnvironment() === Util::ENVIRONMENT_SANDBOX) {
				$twistoData = Twisto::getDataSandbox();
			}
		}

		$orderData = Util::prepareData(
			Util::convertAmountToFractional($order->getGrandTotal()),
			$order->getOrderCurrency()->getCode(),
			$order->getIncrementId(),
			'',
			$order->getBillingAddress()->getFirstname(),
			$order->getBillingAddress()->getLastname(),
			$order->getCustomerEmail(),
			$order->getBillingAddress()->getTelephone(),
			$this->urlBuilder->getUrl('checkout/onepage/success/'),
			$this->urlBuilder->getUrl('checkout/onepage/failure/'),
			$this->urlBuilder->getUrl('checkout/onepage/failure/'),
			$this->getConfigValue('payment/imoje_twisto/version') . ';magento23_' . $version,
			$twistoData
		);

		return $payment->buildOrderForm($orderData, __('Continue'));
	}

	/**
	 * @return integer
	 */
	public function getOrderId()
	{
		return $this->registry->registry('orderId');
	}

	/**
	 * Render form with order information prepared for Payment Page
	 *
	 * @param string $id
	 *
	 * @return bool|\Imoje\Twisto\Model\Sales\Order
	 */
	public function getOrderObject($id = '')
	{
		if(empty($id)) {
			$id = $this->getOrderId();
		}

		return $this->order->loadOrderById($id);
	}

	/**
	 * @return string
	 */
	public function getTwistoDataFromPost()
	{
		$response = '';
		if(isset($_POST['twistoData'])) {
			$response = $_POST['twistoData'];
		}

		return $response;
	}

	/**
	 * Get twisto payload.
	 *
	 * @param Mage_Sales_Model_Order $order
	 *
	 * @return string|null
	 */
	public function getTwistoPayload($order)
	{

		$paymentMethods = $this->getPaymentMethods();

		$previousOrders = $this->getConfigValue('payment/imoje_twisto/purchase_history')
			? $this->getPreviousOrders($order->getCustomerEmail())
			: [];

		return Twisto::getPayload($paymentMethods['twisto']['sk'],
			$order->getGrandTotal(),
			$this->getCart($order),
			$order->getCustomerEmail(),
			\DateTime::createFromFormat('Y-m-d H:i:s', $order->getCreatedAt()),
			$previousOrders);
	}

	/**
	 * @return string
	 */
	public function getOrderIdFromPost()
	{
		$id = '';
		if(isset($_POST['orderId'])) {
			$id = $_POST['orderId'];
		}

		return $id;
	}

	/**
	 * @param Order $order
	 *
	 * @return array
	 */
	private function getCart($order)
	{

		$items = [];

		foreach($order->getAllItems() as $item) {

			$items[] = [
				'name'      => $item->getName(),
				'productId' => $item->getId(),
				'quantity'  => (float) $item->getQtyOrdered(),
				'amount'    => $item->getPriceInclTax(),
				'vatRate'   => (float) $item->getTaxPercent(),
			];
		}

		$addressBilling = $order->getBillingAddress();
		$addressDelivery = $order->getShippingAddress();

		$shippingTaxRate = '';

		foreach($this->taxItem->getTaxItemsByOrderId($order->getId()) as $item) {
			if($item['taxable_item_type'] === 'shipping') {
				$shippingTaxRate = $item['tax_percent'];
			}
		}

		$data = [
			'shipping'        => [
				'amount'  => $order->getShippingInclTax(),
				'vatRate' => (float) $shippingTaxRate,
				'name'    => $order->getShippingDescription(),
			],
			'items'           => $items,
			'addressBilling'  => Util::formatAddress(
				$addressBilling->getFirstname() . ' ' . $addressBilling->getLastname(),
				$addressBilling->getStreet()[0] . (empty($addressBilling->getStreet()[1])
					? ''
					: ', ' . $addressBilling->getStreet()[1]),
				$addressBilling->getCity(),
				$addressBilling->getPostcode(),
				$addressBilling->getCountryId(),
				$addressBilling->getTelephone(),
				$addressBilling->getRegion()
					? $addressBilling->getRegion()
					: ''),
			'addressDelivery' => Util::formatAddress(
				$addressDelivery->getFirstname() . ' ' . $addressDelivery->getLastname(),
				$addressDelivery->getStreet()[0] . (empty($addressDelivery->getStreet()[1])
					? ''
					: ', ' . $addressDelivery->getStreet()[1]),
				$addressDelivery->getCity(),
				$addressDelivery->getPostcode(),
				$addressDelivery->getCountryId(),
				$addressDelivery->getTelephone(),
				$addressDelivery->getRegion()
					? $addressDelivery->getRegion()
					: ''),
		];

		$discount = abs($order->getBaseDiscountAmount());  //use abs because function getDiscountAmount return value with '-'.

		if($discount > 0) {
			$data['discount'] = [
				'amount'  => $discount,
				'name'    => 'discount',
				'vatRate' => 0,
			];
		}

		return $data;
	}

	/**
	 * @param string $value
	 *
	 * @return string
	 */
	private function getConfigValue($value)
	{
		return $this->scopeConfig->getValue($value, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
}

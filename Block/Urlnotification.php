<?php

namespace Imoje\Twisto\Block;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Urlnotification
 *
 * @package Imoje\Twisto\Block
 */
class Urlnotification extends Field
{

	/**
	 * @param AbstractElement $element
	 *
	 * @return string
	 */
	protected function _getElementHtml(AbstractElement $element)
	{
		return $this->getBaseUrl() . 'imoje_twisto/payment/notification';
	}

}

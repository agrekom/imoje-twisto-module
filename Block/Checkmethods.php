<?php

namespace Imoje\Twisto\Block;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Checkmethods
 */
class Checkmethods extends Field
{

	/**
	 * @param AbstractElement $element
	 *
	 * @return string
	 * @throws LocalizedException
	 */
	protected function _getElementHtml(AbstractElement $element)
	{

		$button = $this->getLayout()->createBlock(
			'Magento\Backend\Block\Widget\Button'
		)->setData(
			[
				'id'      => 'collect_button',
				'label'   => __('Activate Twisto'),
				'onclick' => 'javascript:location.href = "' . $this->getBaseUrl() . 'imoje_twisto/payment/checkmethods"',

			]
		);

		$objectManager = ObjectManager::getInstance();
		$scopeConfigInterface = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');
		$writerInterface = $objectManager->get('Magento\Framework\App\Config\Storage\WriterInterface');

		$notificationMessage = '';

		if($scopeConfigInterface->getValue('payment/imoje_twisto/success_tmp', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
			$notificationMessage = '<br><br>' . __('Configuration was successfully downloaded.');

			if($scopeConfigInterface->getValue('payment/imoje_twisto/no_twisto', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
				$notificationMessage = '<br><br>' . __('Configuration was successfully downloaded, but Twisto is not activated in your shop. Contact with Customer Service.');

				$writerInterface->save('payment/imoje_twisto/no_twisto', 0, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
			}

			$writerInterface->save('payment/imoje_twisto/success_tmp', 0, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
		}

		if($scopeConfigInterface->getValue('payment/imoje_twisto/error_tmp', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
			$notificationMessage = '<br><br>' . __('Couldn\'t connect to imoje to get the Twisto configuration. Please try again later!"');

			$writerInterface->save('payment/imoje_twisto/error_tmp', 0, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
		}

		return $button->toHtml() . $notificationMessage;
	}
}

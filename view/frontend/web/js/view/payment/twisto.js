/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'imoje_twisto',
                component: 'Imoje_Twisto/js/view/payment/method-renderer/twisto'
            }
        );
        return Component.extend({});
    }
);

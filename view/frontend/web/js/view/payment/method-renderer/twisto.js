define(
	[
		'jquery',
		'Magento_Checkout/js/view/payment/default',
		'Magento_Customer/js/model/customer',
		'Magento_Checkout/js/action/place-order'
	],
	function ($, Component, customer, placeOrderAction) {
		'use strict';

		return Component.extend({
			defaults:          {
				redirectAfterPlaceOrder: false,
				template:                'Imoje_Twisto/payment/imoje-twisto.html'
			},
			getData:           function () {
				return {
					"method": this.item.method,
				};
			},
			placeOrder:        function (data, event) {
				if (event) {
					event.preventDefault();
				}
				var self = this,
					placeOrder;

				self.isPlaceOrderActionAllowed(false);
				placeOrder = placeOrderAction(this.getData(), this.redirectAfterPlaceOrder);

				$.when(placeOrder).done(function () {
					$.mage.redirect(window.checkoutConfig.payment.imoje_twisto.redirectUrl);
				}).fail(function () {
					self.isPlaceOrderActionAllowed(true);
				});
				return true;
			},
			getPaymentLogoSrc: function () {
				return window.checkoutConfig.payment.imoje_twisto.paymentLogoSrc
			},
		});
	}
);
